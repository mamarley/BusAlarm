package com.michaelmarley.busalarm;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TranslocStop{
	
	@JsonProperty("code")
	public int code;
	
	@JsonProperty("description")
	public String description;
	
	@JsonProperty("url")
	public String url;
	
	@JsonProperty("parent_station_id")
	public String parentStationId;
	
	@JsonProperty("agency_ids")
	public List<Integer> agencyIds;
	
	@JsonProperty("station_id")
	public String stationId;
	
	@JsonProperty("location_type")
	public String locationType;
	
	@JsonProperty("location")
	public TranslocLocation location;
	
	@JsonProperty("stop_id")
	public int stopId;
	
	@JsonProperty("routes")
	public List<Integer> routes;
	
	@JsonProperty("name")
	public String name;
	
	public String toString(){
		return name;
	}
}
