package com.michaelmarley.busalarm;

import java.io.IOException;
import java.util.Calendar;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class BusAlarmService extends Service{
	static SharedPreferences settings;
	
	@Override
	public int onStartCommand(Intent intent,int flags,int startId){
		settings=PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		new AlarmCheckTask().execute();
		return Service.START_NOT_STICKY;
	}
	
	@Override
	public IBinder onBind(Intent intent){
		return null;
	}
	
	private class AlarmCheckTask extends AsyncTask<Void,String,Boolean>{
		@Override
		protected Boolean doInBackground(Void... trash){
			int advanceTime=settings.getInt("AdvanceTime",0);
			if(settings.getBoolean("Weather",false)){
				try{
					DocumentBuilderFactory dbFactory=DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder=dbFactory.newDocumentBuilder();
					Document doc=dBuilder.parse("http://www.google.com/ig/api?weather=,,,"+e6encode(settings.getString("Lat","0"))+","+e6encode(settings.getString("Lng","0"))+"&hl=en&referrer=googlecalendar");
					doc.getDocumentElement().normalize();
					NodeList currentConditionList=doc.getElementsByTagName("condition");
					Element eElement=(Element)currentConditionList.item(0);
					String currentCondition=eElement.getAttribute("data");
					if(!currentCondition.toLowerCase().contains("snow")&&!currentCondition.toLowerCase().contains("freez")&&!currentCondition.toLowerCase().contains("sleet")&&!currentCondition.toLowerCase().contains("icy")&&!currentCondition.toLowerCase().contains("hail")){
						advanceTime=0;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			try{
				Long transitTime;
				TranslocArrivalEstimates arrivalEstimates=new ObjectMapper().readValue(BusAlarm.httpRequest("http://api.transloc.com/1.1/arrival-estimates.json?agencies="+settings.getInt("Agency",0)+"&routes="+settings.getInt("Route",0)+"&stops="+settings.getInt("StartStop",0)+","+settings.getInt("EndStop",0)),TranslocArrivalEstimates.class);
				Calendar calendar=Calendar.getInstance();
				calendar.set(Calendar.HOUR_OF_DAY,settings.getInt("Hour",0));
				calendar.set(Calendar.MINUTE,settings.getInt("Minute",0));
				calendar.set(Calendar.SECOND,0);
				calendar.set(Calendar.MILLISECOND,0);
				Long desiredArrivalTime=calendar.getTime().getTime();
				Long currentTime=Calendar.getInstance().getTime().getTime();
				
				if(arrivalEstimates.data.size()==2){
					for(TranslocArrival firstArrival:arrivalEstimates.data.get(0).arrivals){
						for(TranslocArrival secondArrival:arrivalEstimates.data.get(1).arrivals){
							if(firstArrival.vehicleId==secondArrival.vehicleId){
								if(firstArrival.arrivalAt.getTime()>secondArrival.arrivalAt.getTime()){
									transitTime=firstArrival.arrivalAt.getTime()-secondArrival.arrivalAt.getTime();
								}else{
									transitTime=secondArrival.arrivalAt.getTime()-firstArrival.arrivalAt.getTime();
								}
								System.out.println("Current Time: "+currentTime+" Transit Time: "+transitTime+" Advance Time: "+advanceTime+" Estimated Arrival Time: "+(currentTime+transitTime+advanceTime)+" Desired Arrival Time: "+desiredArrivalTime);
								if(currentTime+transitTime>=desiredArrivalTime){
									return true;
								}else{
									return false;
								}
							}
						}
					}
				}else{
					System.out.println("Not enough data right now.");
				}
			}catch(JsonParseException e){
				e.printStackTrace();
				return null;
			}catch(JsonMappingException e){
				e.printStackTrace();
				return null;
			}catch(IOException e){
				e.printStackTrace();
				return null;
			}catch(NullPointerException e){
				e.printStackTrace();
				return null;
			}
			return false;
		}
		
		@Override
		protected void onPostExecute(Boolean alarm){
			if(alarm==null){
				Toast.makeText(getApplicationContext(),"Application bug.  Yell at the developer.",Toast.LENGTH_LONG).show();
			}else if(alarm.booleanValue()==true){
				Intent intent=new Intent(getBaseContext(),BusAlarmAlarm.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				getApplication().startActivity(intent);
			}
		}
	}
	
	private String e6encode(String input){
		String split[]=input.split(Pattern.quote("."));
		if(split.length!=2){
			throw new NumberFormatException("You must have one and only one decimal point in the number!");
		}
		if(split[0].length()<2){
			while(split[0].length()<2){
				split[0]="0"+split[0];
			}
		}
		if(split[1].length()>6){
			split[1]=split[1].substring(0,5);
		}else if(split[1].length()<6){
			while(split[1].length()<6){
				split[1]=split[1]+"0";
			}
		}
		return split[0]+split[1];
	}
}
