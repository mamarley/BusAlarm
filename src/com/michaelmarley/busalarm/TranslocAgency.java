package com.michaelmarley.busalarm;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TranslocAgency{
	
	@JsonProperty("name")
	public String name;
	
	@JsonProperty("short_name")
	public String shortName;
	
	@JsonProperty("phone")
	public String phone;
	
	@JsonProperty("language")
	public String language;
	
	@JsonProperty("agency_id")
	public int agencyId;
	
	@JsonProperty("long_name")
	public String longName;
	
	@JsonProperty("bounding_box")
	public List<TranslocLocation> boundingBox;
	
	@JsonProperty("url")
	public String url;
	
	@JsonProperty("timezone")
	public String timezone;
	
	@JsonProperty("position")
	public TranslocLocation position;
	
	public String toString(){
		return longName;
	}
}
