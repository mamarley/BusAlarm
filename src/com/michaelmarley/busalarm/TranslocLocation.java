package com.michaelmarley.busalarm;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TranslocLocation{
    
	@JsonProperty("lat")
	public Double lat;
	
	@JsonProperty("lng")
	public Double lng;
}
