package com.michaelmarley.busalarm;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TranslocArrivalEstimate{
	
	@JsonProperty("arrivals")
	public List<TranslocArrival> arrivals;
	
	@JsonProperty("agency_id")
	public int agencyId;
	
	@JsonProperty("stop_id")
	public int stopId;
}
