package com.michaelmarley.busalarm;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.widget.Toast;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

public class BusAlarmAlarm extends Activity{
	static SharedPreferences settings;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_busalarmalarm);
		settings=PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		Intent intent=new Intent(getBaseContext(),BusAlarmService.class);
		final PendingIntent pintent=PendingIntent.getService(getBaseContext(),0,intent,0);
		final AlarmManager alarm=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
		alarm.cancel(pintent);
		Uri notification=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
		final Ringtone r=RingtoneManager.getRingtone(getApplicationContext(),notification);
		if(settings.getBoolean("Ring",true)){
			try{
				r.play();
			}catch(Exception e){
				Toast.makeText(getBaseContext(),"Failed to play default ringtone; perhaps there are no ringtones installed?",Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
		final Vibrator v=(Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
		if(settings.getBoolean("Vibrate",true)){
			try{
				long[] pattern={0,200,500};
				v.vibrate(pattern,0);
			}catch(Exception e){
				Toast.makeText(getBaseContext(),"Failed to initialize vibrator; perhaps your device doesn't have one?",Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
		AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(BusAlarmAlarm.this);
		alertDialogBuilder.setTitle("Bus Alarm!");
		alertDialogBuilder
			.setMessage("Go to the bus stop now.")
			.setCancelable(false)
			.setPositiveButton("Acknowledge",new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog,int id){
					if(r!=null){
						r.stop();
					}
					if(v!=null){
						v.cancel();
					}
					dialog.dismiss();
					finish();
				}
			});
		AlertDialog alertDialog=alertDialogBuilder.create();
		alertDialog.show();
	}
}
