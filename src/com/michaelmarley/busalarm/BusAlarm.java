package com.michaelmarley.busalarm;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

public class BusAlarm extends Activity{
	private int currentAgency;
	private int currentRoute;
	private ArrayList<TranslocStop> fullStopList=new ArrayList<TranslocStop>();
	static SharedPreferences settings;
	static SharedPreferences.Editor editor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_busalarm);
		settings=PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		editor=settings.edit();
		Intent intent=new Intent(getBaseContext(),BusAlarmService.class);
		final PendingIntent pintent=PendingIntent.getService(getBaseContext(),0,intent,0);
		final AlarmManager alarm=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
		final Button setButton=(Button)findViewById(R.id.set_alarm_button);
		setButton.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v){
				Spinner agencySelection=(Spinner)findViewById(R.id.agency_selection);
				Spinner routeSelection=(Spinner)findViewById(R.id.route_selection);
				Spinner startStopSelection=(Spinner)findViewById(R.id.startstop_selection);
				Spinner endStopSelection=(Spinner)findViewById(R.id.endstop_selection);
				if(agencySelection.getSelectedItem()==null){
					Toast.makeText(getBaseContext(),"You must select a transit system.",Toast.LENGTH_LONG).show();
				}else if(routeSelection.getSelectedItem()==null){
					Toast.makeText(getBaseContext(),"You must select a route.",Toast.LENGTH_LONG).show();
				}else if(startStopSelection.getSelectedItem()==null){
					Toast.makeText(getBaseContext(),"You must select a start stop.",Toast.LENGTH_LONG).show();
				}else if(endStopSelection.getSelectedItem()==null){
					Toast.makeText(getBaseContext(),"You must select an end stop.",Toast.LENGTH_LONG).show();
				}else{
					editor.commit();
					alarm.setRepeating(AlarmManager.RTC_WAKEUP,Calendar.getInstance().getTimeInMillis(),60*1000,pintent);
				}
			}
		});
		final Button cancelButton=(Button)findViewById(R.id.cancel_alarm_button);
		cancelButton.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v){
				alarm.cancel(pintent);
			}
		});
		final TimePicker timePicker=(TimePicker)findViewById(R.id.alarm_time);
		editor.putInt("Hour",timePicker.getCurrentHour());
		editor.putInt("Minute",timePicker.getCurrentMinute());
		timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener(){
			public void onTimeChanged(TimePicker timePicker,int hour,int minute){
				editor.putInt("Hour",hour);
				editor.putInt("Minute",minute);
			}  
		});
		final Spinner advanceTimeSelection=(Spinner)findViewById(R.id.advance_time_selection);
		ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(this,R.array.advance_time_array,android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		advanceTimeSelection.setAdapter(adapter);
		advanceTimeSelection.setOnItemSelectedListener(new OnItemSelectedListener(){
			@Override
			public void onItemSelected(AdapterView<?> parent,View view,int pos,long id){
				editor.putInt("AdvanceTime",Integer.parseInt((String)parent.getItemAtPosition(pos)));
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0){}
		});
		final CheckBox weatherBox=(CheckBox)findViewById(R.id.weather_control_checkbox);
		editor.putBoolean("Weather",weatherBox.isChecked());
		weatherBox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton ringBox,boolean checked) {
				editor.putBoolean("Weather",checked);
			}  
		});
		final CheckBox ringBox=(CheckBox)findViewById(R.id.ring_control_checkbox);
		editor.putBoolean("Ring",ringBox.isChecked());
		ringBox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton ringBox,boolean checked) {
				editor.putBoolean("Ring",checked);
			}  
		});
		final CheckBox vibrateBox=(CheckBox)findViewById(R.id.vibrate_control_checkbox);
		editor.putBoolean("Vibrate",vibrateBox.isChecked());
		vibrateBox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton ringBox,boolean checked) {
				editor.putBoolean("Vibrate",checked);
			}  
		});
		new AgenciesRequestTask().execute();
	}
	
	public static String httpRequest(String uri){
		HttpClient httpclient=new DefaultHttpClient();
		HttpResponse response;
		String responseString=null;
		try{
			response=httpclient.execute(new HttpGet(uri));
			StatusLine statusLine=response.getStatusLine();
			if(statusLine.getStatusCode()==HttpStatus.SC_OK){
				ByteArrayOutputStream out=new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString=out.toString();
			}else{
				response.getEntity().getContent().close();
				throw new IOException(statusLine.getReasonPhrase());
			}
		}catch(ClientProtocolException e){
			//TODO Handle problems..
		}catch (IOException e){
			//TODO Handle problems..
		}
		return responseString;
	}
	
	private class AgenciesRequestTask extends AsyncTask<Void,String,TranslocAgencies>{
		@Override
		protected TranslocAgencies doInBackground(Void... trash){
			try{
				return new ObjectMapper().readValue(httpRequest("http://api.transloc.com/1.1/agencies.json"),TranslocAgencies.class);
			}catch(JsonParseException e){
				e.printStackTrace();
				return null;
			}catch(JsonMappingException e){
				e.printStackTrace();
				return null;
			}catch(IOException e){
				e.printStackTrace();
				return null;
			}catch(NullPointerException e){
				e.printStackTrace();
				return null;
			}
		}
		
		@Override
		protected void onPostExecute(final TranslocAgencies agencyList){
			if(agencyList==null){
				Toast.makeText(getBaseContext(),"Error parsing agency data, perhaps your Internet connection isn't working?",Toast.LENGTH_LONG).show();
			}else{
				Spinner agencySelection=(Spinner)findViewById(R.id.agency_selection);
				agencySelection.setOnItemSelectedListener(new OnItemSelectedListener(){
					@Override
					public void onItemSelected(AdapterView<?> parent,View view,int pos,long id){
						currentAgency=agencyList.data.get(pos).agencyId;
						editor.putInt("Agency",currentAgency);
						editor.putString("Lat",agencyList.data.get(pos).position.lat.toString());
						editor.putString("Lng",agencyList.data.get(pos).position.lng.toString());
						new RoutesRequestTask().execute();
						new StopsRequestTask().execute();
					}
					@Override
					public void onNothingSelected(AdapterView<?> arg0){}
				});
				ArrayAdapter<TranslocAgency> adapter=new ArrayAdapter<TranslocAgency>(getBaseContext(),android.R.layout.simple_list_item_1,android.R.id.text1,agencyList.data);
				agencySelection.setAdapter(adapter);
			}
		}
	}
	
	private class RoutesRequestTask extends AsyncTask<Void,String,ArrayList<TranslocRoute>>{
		@Override
		protected ArrayList<TranslocRoute> doInBackground(Void... trash){
			try{
				Map<String,Object> routeMap=new ObjectMapper().readValue(httpRequest("http://api.transloc.com/1.1/routes.json?agencies="+currentAgency),Map.class);
				Map<String,Object> agencyMap=(Map)routeMap.get("data");
				List<Map<String,Object>> routeList=(List)agencyMap.get(Integer.toString(currentAgency));
				final ArrayList<TranslocRoute> routes=new ArrayList<TranslocRoute>();
				for(Map<String,Object> route:routeList){
					routes.add(new TranslocRoute(Integer.parseInt((String)route.get("route_id")),(String)route.get("short_name"),(String)route.get("long_name")));
				}
				return routes;
			}catch(JsonParseException e){
				e.printStackTrace();
				return null;
			}catch(JsonMappingException e){
				e.printStackTrace();
				return null;
			}catch(IOException e){
				e.printStackTrace();
				return null;
			}catch(NullPointerException e){
				e.printStackTrace();
				return null;
			}
		}
		
		@Override
		protected void onPostExecute(final ArrayList<TranslocRoute> routes){
			if(routes==null){
				Toast.makeText(getBaseContext(),"Error parsing route data, perhaps no routes are in service or your Internet connection isn't working?",Toast.LENGTH_LONG).show();
			}else{
				Spinner routeSelection=(Spinner)findViewById(R.id.route_selection);
				routeSelection.setOnItemSelectedListener(new OnItemSelectedListener(){
					@Override
					public void onItemSelected(AdapterView<?> parent,View view,int pos,long id){
						currentRoute=routes.get(pos).id;
						editor.putInt("Route",currentRoute);
						new FilterStopListTask().execute(fullStopList);
					}
					@Override
					public void onNothingSelected(AdapterView<?> arg0){}
				});
				ArrayAdapter<TranslocRoute> adapter=new ArrayAdapter<TranslocRoute>(getBaseContext(),android.R.layout.simple_list_item_1,android.R.id.text1,routes);
				routeSelection.setAdapter(adapter);
			}
		}
	}
	
	private class StopsRequestTask extends AsyncTask<Void,String,TranslocStops>{
		@Override
		protected TranslocStops doInBackground(Void... trash){
			try{
				return new ObjectMapper().readValue(httpRequest("http://api.transloc.com/1.1/stops.json?agencies="+currentAgency),TranslocStops.class);
			}catch(JsonParseException e){
				e.printStackTrace();
				return null;
			}catch(JsonMappingException e){
				e.printStackTrace();
				return null;
			}catch(IOException e){
				e.printStackTrace();
				return null;
			}catch(NullPointerException e){
				e.printStackTrace();
				return null;
			}
		}
		
		@Override
		protected void onPostExecute(TranslocStops stopList){
			fullStopList.clear();
			if(stopList!=null){
				fullStopList.addAll(stopList.data);
			}else{
				Toast.makeText(getBaseContext(),"Error parsing stop data, perhaps your Internet connection isn't working?",Toast.LENGTH_LONG).show();
			}
			new FilterStopListTask().execute(fullStopList);
		}
	}
	
	private class FilterStopListTask extends AsyncTask<ArrayList<TranslocStop>,String,ArrayList<TranslocStop>>{
		@Override
		protected ArrayList<TranslocStop> doInBackground(ArrayList<TranslocStop>... fullStopList){
			if(fullStopList.length==1){
				if(fullStopList[0]==null){
					System.out.println("Null!");
				}
				ArrayList<TranslocStop> currentRouteStopList=new ArrayList<TranslocStop>();
				for(int i=fullStopList[0].size()-1;i>=0;i--){
					if(fullStopList[0].get(i).routes.contains(currentRoute)){
						currentRouteStopList.add(fullStopList[0].get(i));
					}
				}
				return currentRouteStopList;
			}else{
				throw new IllegalArgumentException("Sorry, one arg only.");
			}
		}
		
		@Override
		protected void onPostExecute(final ArrayList<TranslocStop> currentRouteStopList){
			Spinner startStopSelection=(Spinner)findViewById(R.id.startstop_selection);
			Spinner endStopSelection=(Spinner)findViewById(R.id.endstop_selection);
			startStopSelection.setOnItemSelectedListener(new OnItemSelectedListener(){
				@Override
				public void onItemSelected(AdapterView<?> parent,View view,int pos,long id){
					editor.putInt("StartStop",currentRouteStopList.get(pos).stopId);
				}
				@Override
				public void onNothingSelected(AdapterView<?> arg0){}
			});
			endStopSelection.setOnItemSelectedListener(new OnItemSelectedListener(){
				@Override
				public void onItemSelected(AdapterView<?> parent,View view,int pos,long id){
					editor.putInt("EndStop",currentRouteStopList.get(pos).stopId);
				}
				@Override
				public void onNothingSelected(AdapterView<?> arg0){}
			});
			ArrayAdapter<TranslocStop> startAdapter=new ArrayAdapter<TranslocStop>(getBaseContext(),android.R.layout.simple_list_item_1,android.R.id.text1,currentRouteStopList);
			ArrayAdapter<TranslocStop> endAdapter=new ArrayAdapter<TranslocStop>(getBaseContext(),android.R.layout.simple_list_item_1,android.R.id.text1,currentRouteStopList);
			startStopSelection.setAdapter(startAdapter);
			endStopSelection.setAdapter(endAdapter);
		}
	}
}
